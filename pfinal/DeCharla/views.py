import random
import json
import string
from django.core import validators
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Session, Room, Message, Password
from django.utils import timezone
import xml.etree.ElementTree as ET
import datetime

# PARTE OBLIGATORIA
def get_statistics():
    text_messages = Message.objects.filter(is_image=False).count()  # Cuenta los mensajes de texto
    image_messages = Message.objects.filter(is_image=True).count()  # Cuenta los mensajes de imagen
    active_rooms = Room.objects.count()  # Cuenta las salas activas
    return text_messages, image_messages, active_rooms  # Devuelve las estadísticas

def check_session(request):
    cookie = request.COOKIES.get("Sesion")  # Obtiene el valor de la cookie "Sesion"
    if cookie is None:  # Si no hay cookie
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))  # Genera un valor aleatorio
        response = HttpResponseRedirect("/login")  # Redirecciona a la página de inicio de sesión
        response.set_cookie("Sesion", cookie)  # Establece la cookie con el valor aleatorio
        return response  # Devuelve la respuesta de redirección
    if request.path != "/login":  # Si la ruta no es "/login"
        check = Session.objects.filter(Sesion=cookie).exists()  # Verifica si existe una sesión con el valor de la cookie
    else:
        check = True  # Si la ruta es "/login", se considera como sesión válida
    if not check:  # Si no se encuentra una sesión válida
        return HttpResponseRedirect("/login")  # Redirecciona a la página de inicio de sesión
    return None  # Si la sesión es válida, devuelve None

def get_rooms(request):
    room_lists = Room.objects.all()  # Obtiene todas las salas
    for sala in room_lists:
        total_messages = Message.objects.filter(Sala=sala).count()  # Cuenta los mensajes totales de la sala
        last_visit = request.session.get(f"last_visit_{sala.Nombre}")  # Obtiene la última visita de la sala desde la sesión
        if last_visit:
            if isinstance(last_visit, str):
                last_visit = datetime.datetime.strptime(last_visit, "%Y-%m-%d %H:%M:%S")  # Convierte a objeto datetime
            last_visit_messages = Message.objects.filter(Sala=sala, Fecha__gt=last_visit).exclude(
                Autor__Sesion=request.COOKIES.get("Sesion")).count()  # Cuenta los mensajes desde la última visita del usuario
        else:
            last_visit_messages = total_messages  # Si no hay última visita, se consideran todos los mensajes como nuevos
        sala.total_messages = total_messages  # Agrega la cantidad total de mensajes a la sala
        sala.last_visit_messages = last_visit_messages  # Agrega la cantidad de mensajes desde la última visita a la sala
        request.session[f"last_visit_{sala.Nombre}"] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")  # Actualiza la última visita en la sesión
    return room_lists  # Devuelve la lista de salas actualizada

@csrf_exempt
def login(request):
    if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
        psswrd = request.POST.get("password")  # Obtiene el valor del campo "password" del formulario
        check_psswrd = Password.objects.filter(Password=psswrd).exists()  # Verifica si la contraseña existe
        if check_psswrd:  # Si la contraseña es válida
            session = Session()  # Crea un nuevo objeto de sesión
            session.Sesion = request.COOKIES.get("Sesion")  # Obtiene el valor de la cookie "Sesion"
            session.save()  # Guarda la sesión en la base de datos
            return HttpResponseRedirect("/")  # Redirecciona a la página index

    cookie = request.COOKIES.get("Sesion")  # Obtiene el valor de la cookie "Sesion"
    check = Session.objects.filter(Sesion=cookie).exists()  # Verifica si existe una sesión con el valor de la cookie
    if check:  # Si existe una sesión válida
        return HttpResponseRedirect("/")  # Redirecciona a la página index
    login_html = render(request, "login.html")  # Renderiza la plantilla "login.html"
    return login_html  # Devuelve la respuesta HTML generada por la plantilla


#Lo que hace csrf_exempt es saltar el mecanismo de seguridad por ejemplo si no tenemos esto, en el index y intentamos hacer un post en esta pagina no vamos a poder
@csrf_exempt
def index(request):
    response = check_session(request)
    if response:
        return response

    if request.method == "POST":
        room_name = request.POST.get("sala")
        nombre_existente = Room.objects.filter(Nombre=room_name).exists()
        if not nombre_existente:
            sesion = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))
            sala_auxiliar = Room(Nombre=room_name, Autor=sesion)
            sala_auxiliar.save()
            sala_html = HttpResponseRedirect("/" + room_name)
            return sala_html

    text_messages, image_messages, active_rooms, = get_statistics()  # Obtiene las estadísticas
    sesion = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))
    if sesion.Nombre:
        nombre_usuario = sesion.Nombre
    else:
        nombre_usuario = 'Anonimo'
    index_html = render(request, "index.html", {
        "rooms": get_rooms(request),
        "session": sesion,
        "text_messages": text_messages,
        "image_messages": image_messages,
        "active_rooms": active_rooms,
        "user_name": nombre_usuario,
    })
    return index_html

# Añadir XML
@csrf_exempt
def room(request, room_id):
    response = check_session(request)  # Verifica la sesión del usuario
    if response:  # Si hay una respuesta de sesión inválida
        return response  # Devuelve la respuesta sin continuar con el resto del código

    login_html = HttpResponseRedirect("/login")  # Redirecciona a la página de inicio de sesión
    sesion = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))

    try:
        sala = Room.objects.get(Nombre=room_id)  # Obtiene la sala con el nombre proporcionado
        mensajes = Message.objects.filter(Sala=sala).order_by('-Fecha')  # Obtiene los mensajes de la sala ordenados por fecha descendente
        text_messages, image_messages, active_rooms, = get_statistics()  # Obtiene las estadísticas

        if request.method == "PUT":
            xml_string = request.body.decode('utf-8')
            xml_messages = xml(xml_string)
            autor, created = Session.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))  # Obtiene o crea la sesión del usuario
            for message in xml_messages:
                message = Message(Texto=message['text'], is_image=message['isimg'], Autor=autor, Sala=sala)  # Crea un nuevo mensaje
                message.save()  # Guarda el mensaje en la base de datos

        if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
            texto = request.POST.get("mensaje")  # Obtiene el texto del mensaje del formulario
            is_image = bool(request.POST.get("is_image", False))  # Verifica si hay una imagen adjunta
            autor, created = Session.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))  # Obtiene o crea la sesión del usuario

            sesion = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))
            mensaje = Message(Texto=texto, is_image=is_image, Autor=autor, Sala=sala)  # Crea un nuevo mensaje

            if is_image:
                mensaje.url_image = texto
            mensaje.save()  # Guarda el mensaje en la base de datos

        room_html = render(request, "room.html", {  # Renderiza la plantilla "room.html" con los siguientes valores de contexto
            "room_id": room_id,  # ID de la sala
            "mensajes": mensajes,  # Mensajes de la sala
            "session": sesion,
            "rooms": get_rooms(request),  # Lista de salas
            "text_messages": text_messages,
            "image_messages": image_messages,
            "active_rooms": active_rooms,
        })

        return room_html  # Devuelve la respuesta HTML generada por la plantilla
    except Room.DoesNotExist:  # Si no se encuentra la sala
        return login_html  # Redirecciona a la página de inicio de sesión

@csrf_exempt
def dynamic_room(request, room_id):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    session = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))  # Obtiene la sesión actual
    text_messages, image_messages, active_rooms, = get_statistics()  # Obtiene las estadísticas

    login_html = HttpResponseRedirect("/login")  # Redirecciona a la página de inicio de sesión por defecto
    try:
        sala = Room.objects.get(Nombre=room_id)  # Obtiene la sala por su nombre
        mensajes = Message.objects.filter(Sala=sala).order_by('-Fecha')  # Obtiene los mensajes de la sala

        if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
            texto = request.POST.get("mensaje")  # Obtiene el texto del mensaje del formulario
            is_image = bool(request.POST.get("is_image", False))  # Verifica si hay una imagen adjunta
            autor, created = Session.objects.get_or_create(
                Sesion=request.COOKIES.get("Sesion"))  # Obtiene o crea la sesión del usuario

            mensaje = Message(Texto=texto, is_image=is_image, Autor=autor, Sala=sala)  # Crea un nuevo mensaje

            if is_image:
                mensaje.url_image = texto
            mensaje.save()  # Guarda el mensaje en la base de datos

        room_html = render(request, "dinroom.html", {
            "session": session,
            "room_id": room_id,
            "mensajes": mensajes,
            "rooms": get_rooms(request),
            "text_messages": text_messages,
            "image_messages": image_messages,
            "active_rooms": active_rooms,
        })

        return room_html  # Renderiza la plantilla "dinroom.html" con los datos
    except Room.DoesNotExist:  # Si la sala no existe
        return login_html  # Redirecciona a la página de inicio de sesión

def json_room(request, room_id):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    try:
        sala = Room.objects.get(Nombre=room_id)  # Obtiene la sala por su nombre
        mensajes = Message.objects.filter(Sala=sala).order_by('-Fecha')  # Obtiene los mensajes de la sala

        messages_json = []
        for mensaje in mensajes:
            message_data = {
                "author": mensaje.Autor.Nombre,  # Nombre del autor del mensaje
                "text": mensaje.Texto,  # Contenido del mensaje
                "isimg": mensaje.is_image,  # Indicador de si el mensaje es una imagen
                "date": mensaje.Fecha.strftime("%Y-%m-%d %H:%M:%S")  # Fecha y hora del mensaje en formato string
            }
            messages_json.append(message_data)  # Agrega los datos del mensaje al JSON

        return HttpResponse(json.dumps(messages_json), content_type="application/json")  # Devuelve la respuesta con el JSON de los mensajes
    except Room.DoesNotExist:  # Si la sala no existe
        return HttpResponse(status=404)  # Devuelve una respuesta con código de estado 404 (no encontrado)

def xml(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for message in root.findall('message'):
        isimg = message.get('isimg')
        if isimg == "true":
           isimg = True
        else:
            isimg = False
        text = message.find('text').text
        messages.append({'isimg': isimg, 'text': text})
    return messages

def help(request):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    session = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))  # Obtiene la sesión del usuario

    text_messages, image_messages, active_rooms, = get_statistics()  # Obtiene las estadísticas
    help_html = render(request, "help.html", {
        "room_lists": get_rooms(request),
        "text_messages": text_messages,
        "image_messages": image_messages,
        "active_rooms": active_rooms,
        "session": session,
    })  # Renderiza la plantilla "help.html" con los datos
    return help_html  # Devuelve la respuesta HTML generada por la plantilla

@csrf_exempt
def settings(request):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    session = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))  # Obtiene la sesión actual

    if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
        name = request.POST.get("name")  # Obtiene el valor del campo "name" del formulario
        font_size = request.POST.get("font_size")  # Obtiene el valor del campo "font_size" del formulario
        font_type = request.POST.get("font_type")  # Obtiene el valor del campo "font_type" del formulario

        session.Nombre = name  # Actualiza el nombre de la sesión
        session.font_size = font_size
        session.font_type = font_type
        session.save()  # Guarda los cambios en la base de datos

        response = render(request, "settings.html", {
            "session": session,
            "room_lists": get_rooms(request),
        })  # Renderiza la plantilla "settings.html" con los datos actualizados

        return response  # Devuelve la respuesta con los datos actualizados y las cookies establecidas

    text_messages, image_messages, active_rooms = get_statistics()  # Obtiene las estadísticas
    settings_html = render(request, "settings.html", {
        "session": session,
        "room_lists": get_rooms(request),
        "text_messages": text_messages,
        "image_messages": image_messages,
        "active_rooms": active_rooms,
    })  # Renderiza la plantilla "settings.html" con los datos
    return settings_html  # Devuelve la respuesta HTML generada por la plantilla

@csrf_exempt
def findroom(request):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    sala_no_existe = False

    session = Session.objects.get(Sesion=request.COOKIES.get("Sesion"))  # Obtiene la sesión actual
    if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
        room_name = request.POST.get("room_name")  # Obtiene el valor del campo "room_name" del formulario
        if room_name:  # Si se proporcionó un nombre de sala
            try:
                sala = Room.objects.get(Nombre=room_name)  # Busca la sala por su nombre
                return HttpResponseRedirect(f'/{sala.Nombre}')  # Redirecciona a la página de la sala encontrada
            except Room.DoesNotExist:  # Si la sala no existe
                sala_no_existe = True

    text_messages, image_messages, active_rooms = get_statistics()  # Obtiene las estadísticas
    return render(request, "rooms.html",{
                    "session": session,
                    "sala_no_existe": sala_no_existe,
                    "text_messages": text_messages,
                    "image_messages": image_messages,
                    "active_rooms": active_rooms
                    })  # Renderiza la plantilla "rooms.html" con los datos

@csrf_exempt
def deleteroom(request, room_id):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response

    try:
        sala = Room.objects.get(id=room_id)  # Obtiene la sala por su ID
        sala.delete()  # Elimina la sala de la base de datos
    except Room.DoesNotExist:  # Si la sala no existe
        pass  # Continúa sin hacer nada

    return HttpResponseRedirect("/")  # Redirecciona a la página index

# PARTE OPCIONAL
@csrf_exempt
def logout(request):
    if request.method == "POST":  # Si la solicitud es un POST (formulario enviado)
        response = HttpResponseRedirect("/")  # Redirecciona a la página index
        response.delete_cookie("Sesion")  # Elimina la cookie de sesión del cliente
        return response

# PARTE OPCIONAL
def like(request, room_id):
    response = check_session(request)  # Verifica la sesión
    if response:  # Si la sesión no es válida
        return response  # Redirecciona a la página de inicio de sesión

    try:
        sala = Room.objects.get(id=room_id)  # Obtiene la sala por su ID
        sala.Likes += 1  # Incrementa el contador de votos de la sala
        sala.save()  # Guarda los cambios en la base de datos
    except Room.DoesNotExist:  # Si la sala no existe
        return HttpResponseRedirect("/")  # Redirecciona a la página index

    return HttpResponseRedirect("/")  # Redirecciona a la página index

# PARTE OPCIONAL
def get_favicon(request):
    with open("favicon.png", "rb") as favicon:  # Abre el archivo "favicon.png" en modo lectura binaria
        ans = favicon.read()  # Lee el contenido del archivo
    return HttpResponse(ans)  # Devuelve una respuesta con el contenido del archivo

