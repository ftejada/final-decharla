from django.db import models

class Password(models.Model):
    Password = models.CharField(max_length=16)

class Session(models.Model):
    Sesion = models.TextField(unique=True, null=True)
    Nombre = models.TextField(default="User")
    font_size = models.CharField(max_length=10, default="small")
    font_type = models.CharField(max_length=20, default="sans-serif")

class Room(models.Model):
    Nombre = models.CharField(max_length=64)
    Autor = models.ForeignKey(Session, on_delete=models.CASCADE)
    Likes = models.PositiveIntegerField(default=0)

class Message(models.Model):
    Fecha = models.DateTimeField(auto_now_add=True)
    Texto = models.TextField()
    is_image = models.BooleanField(default=False)
    imagen = models.ImageField(upload_to='imagenes/', null=True, blank=True)
    url_image = models.URLField(null=True, blank=True)
    Autor = models.ForeignKey(Session, on_delete=models.CASCADE)
    Sala = models.ForeignKey(Room, on_delete=models.CASCADE)
