# Generated by Django 4.1.7 on 2023-06-11 16:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("DeCharla", "0003_rename_votos_sala_likes"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Sala",
            new_name="Room",
        ),
    ]
