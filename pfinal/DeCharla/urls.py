from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login', views.login, name='login'),
    path('', views.index, name='index'),
    path('logout/', views.logout, name='logout'),
    path('help', views.help, name='help'),
    path('settings', views.settings, name='settings'),

    path('favicon.ico', views.get_favicon, name='favicon'),
    path('findroom', views.findroom, name='findroom'),
    path('<str:room_id>', views.room, name='room'),
    path('<str:room_id>/json', views.json_room, name='json_room'),
    path('<str:room_id>/dynamic', views.dynamic_room, name='dynamic_room'),
    path('like/<int:room_id>', views.like, name='like'),
    path('deleteroom/<int:room_id>', views.deleteroom, name='deleteroom'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


