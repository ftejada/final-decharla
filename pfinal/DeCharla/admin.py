from django.contrib import admin
from .models import Password, Session, Room, Message

admin.site.register(Session)
admin.site.register(Password)
admin.site.register(Room)
admin.site.register(Message)