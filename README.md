# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Fernando Tejada de Galdo
* Titulación: Ingeniería Telemática
* Cuenta en laboratorios: ftejada
* Cuenta URJC: ftejada2016@alumnos.urjc.es
* Video básico (url): https://youtu.be/Sm0mcZRiNbg
* Video parte opcional (url): https://youtu.be/XDl8vtsVExA
* Despliegue (url): http://loftejada.pythonanywhere.com/
* Contraseñas: 1234 / 0000
* Cuenta Admin Site: usuario/contraseña : admin/admin

## Resumen parte obligatoria

La práctica final de la asignatura de Servicios Telemáticos consta de una aplicación llamada DeCharla, la cual consiste en la interacción entre usuarios mediante salas, pudiendo comunicarse en tiempo real todos los usuarios entre sí, y poder debatir y compartir opiniones en diferentes salas. A continuación se muestra un breve resumen de cómo utilizar la aplicación:

- Para empezar a usar DeCharla debemos acceder usando una contraseña válida, las cuales están proporcionadas en los datos del apartado anterior. Una vez logeados, procedemos a empezar.

- Tras el login podremos ver el nombre de la aplicación arriba a la izquierda, seguido de las diferentes opciones disponibles: 

 * *Homepage*: Enlace a la página principal. Aquí veremos las salas que hay disponibles en ese momento, con la correspondiente información de cada una, así como un recuadro en el que podremos crear una sala nosotros mismos. También observaremos un botón que podremos pulsar y nos redirigirá al buscador de salas, donde podremos escribir el nombre de una de ellas y automáticamente nos redirigirá a ella. 
 
 * *Settings*: Enlace a la configuración, donde podremos cambiar el nombre de usuario, junto con el tamaño y tipo de letra de la aplicación.
 
 * *Help*: Enlace a la página de ayuda, donde habrá una breve introducción de la práctica y una descripción del funcionamiento. Es parecido a lo que tenemos aquí, pero de forma más resumida.
 
 * *Admin*: Enlace a la página del administrador. Nos redirigirá al Admin Site, predefinido por Django.

- Tras entrar a una sala, el usuario podrá escribir un mensaje y enviarlo. Si lo desea, podrá añadir una imagen mediante su URL, y para ello hay un checkbox distintivo el cual hay que marcar en caso de que sea una imagen. 

- Las salas tienen dos modos disponibles:
 * *Modo dinámico*: Cada 10 segundos la página se va a refrescar. Esta implementación está hecha con el objetivo de que si hay varios usuarios a la vez, los mensajes lleguen casi instantáneamente.
 
 * *Modo JSON*: Accedemos a la página en modo JSON para ver los mensajes de otra forma. 

- Por último, tenemos un pie de página en el cual se muestran diferentes datos: 
 * Mensajes de texto 
 * Imágenes
 * Salas activas en ese momento

En conclusión, DeCharla es una aplicación sencilla de usar y cómoda la el reparto de opiniones entre los usuarios que la visiten. ¡Disfruten de la charla!

## Lista partes opcionales

* Nombre parte: Inclusión de un favicon del sitio.
* Nombre parte: Permitir que las sesiones autenticadas se puedan terminar. Esto es, que haya una opción para “terminar la sesión”, de forma que al usarla, si se vuelve a acceder al sitio vuelva a recibirse el formulario para autenticarse.
* Nombre parte: Permitir votar las las salas. Para ello, en cada sala aparecerá un botón “Me gusta” para indicar que se da un voto a la sala. Cada sala, en el listado general, saldrá junto a los votos que tiene. 






